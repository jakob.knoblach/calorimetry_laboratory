from typing import List, Tuple
from typing import Dict

import h5py as h5
import matplotlib.pyplot as plt
import numpy as np


def plot_temp_over_time(
    data: List[np.ndarray],
    time: List[np.ndarray],
    
    x_label: str,
    y_label: str, probename
) -> None:
    """Plots temperature data over time with error bars for multiple datasets.

    This function creates a plot representing temperature data against time
    for multiple sensors or datasets. Each dataset's standard deviation is visualized
    with error bars.

     Args:
        data (List[np.ndarray]): A list of numpy arrays where each array represents
                                 the temperature data (with standard deviation).
                                 Each array should have a shape of (2, n), with n
                                 representing the number of data points.
        time (List[np.ndarray]): A list of numpy arrays with the time data corresponding
                                 to each dataset in `data`.
        legend (List[str]): A list of strings that label each dataset in the legend.
        x_label (str): The label for the x-axis (time).
        y_label (str): The label for the y-axis (temperature).

    """
    # init the matplotlib.axes.Axes and matplotlib.figure.Figure Object for later plot
    #fig, ax = plt.subplots(1, 1)
    #markers = ["o", "^", "2", "p", "D"]

    #for i in range(len(data)):
        # TODO: draw a plot using the ax.errorbar(...) function
        # Document: https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.errorbar.html
       
    if probename == "probe constant":            
        x = np.arange(0.1, 76, 0.3)
        y = np.arange(0.1, 25, 0.3)
        fig, ax = plt.subplots()
        ax.errorbar(time, data[0])
        ax.errorbar(time, data[1])
        ax.errorbar(time, data[2])
        ax.errorbar(time, data[3])
        ax.legend(['mittelwert', 'sensor 1', 'sensor 2', 'sensor 3'])

        plt.show()

        x = np.arange(0.1, 76, 0.3)
        y = np.arange(0.1, 30, 0.3)
        fig, ax = plt.subplots()
        ax.errorbar(data[5], data[4])
        ax.legend(['sensor umgebung'])
        
    if probename == "probe1":
        x = np.arange(0.1, 76, 0.3)
        y = np.arange(0.1, 25, 0.3)
        fig, ax = plt.subplots()
        ax.errorbar(time, data[0])
        ax.errorbar(time, data[1])
        ax.errorbar(time, data[2])
        ax.errorbar(time, data[3])
        ax.errorbar(time, data[4])
        ax.legend(['mittelwert', 'sensor 1', 'sensor 2', 'sensor 3','sensor bath'])

        plt.show()

        x = np.arange(0.1, 76, 0.3)
        y = np.arange(0.1, 30, 0.3)
        fig, ax = plt.subplots()
        ax.errorbar(data[6], data[5])
        ax.legend(['sensor umgebung'])
        # DONE #

    # Errorbars removed from Legend
    legend_handles, labels = ax.get_legend_handles_labels()
    legend_handles = [h[0] for h in legend_handles]

    # TODO: set legend, x- and y- axis label.
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    
    # DONE #

    ax.ticklabel_format(scilimits=(0, 3))


def get_plot_data_from_dataset(
    data_path: str, group_path: str, probename, length) -> Dict[str, np.ndarray]:
    """Get the necessary data from the dataset to plot.

    This function returns the data in a HDF5 file in all subgroups of a group in 'group_path'
    and automatically categorizes and names the data based on the name of the dataset as well as the metadata.

    Args:
        data_path (str): path to HDF5 file.
        group_path (str): path in HDF5 to group.

    Returns:
        dict[str, np.ndarray]: Data for plot in a dict.

    Example:
        Output (example data):
        {
            "temperature": np.array([
                            [24.89, 24.92, 24.00, 25.39],
                            [24.89, 24.92, 24.00, 25.39],
                            [24.89, 24.92, 24.00, 25.39]
                        ]) -> temperature from each sensor, The first dimension(row) represents the sensor.
            "timestamp": np.array([
                            [0.43, 1.60, 3.05, 4.25],
                            [0.81, 2.13, 3.49, 4.62],
                            [1.34, 2.60, 3.85, 5.08],
                        ]) -> timestamp for each sensor, The first dimension(row) represents the sensor.
            "name": np.array(["sensor_1", "sensor_2", "sensor_3"]) -> name of each sensor should be hier
        }

    """
    temperature = []
    time = []
    name = []
    import h5py as h5
    with h5.File(data_path) as data:
        group = data[group_path]
        subgroups = []
        min_len = None
        start_time = None

        for subgroup in group:
            try:
                dataset_start_time = group[subgroup]["timestamp"][0]
                dataset_len = len(group[subgroup]["timestamp"])

                # Find the minimum length of the data set.
                if min_len is None:
                    min_len = dataset_len
                elif dataset_len < min_len:
                    min_len = dataset_len

                subgroups.append(subgroup)

                # Only group with dataset called timestamp will be read.
            except KeyError:
                continue

            # TODO: Find the start time point of the measurement.
            if probename == "constant":
                import h5py as h5
                with  h5.File(data_path, "r") as f:
                    sensor_1 = np.array(f["RawData/1ee5ec00-4a00-68a1-bb1e-873c2dd4dbde/timestamp"][0])
                    sensor_2 = np.array(f["RawData/1ee5ec03-7e64-6071-8ca3-98dbab0a7719/timestamp"][0])
                    sensor_3 = np.array(f["RawData/1ee5ec04-30cd-678f-a64b-0ce7544ef5e8/timestamp"][0]) 
                    sensor_4 = np.array(f["RawData/1ee5ec04-c845-69e2-853a-25c11543466f/timestamp"][0]) 
                liste_time = [sensor_1, sensor_2, sensor_3, sensor_4]  
                start_time = min(liste_time)
                
            if probename == "probe":
                with  h5.File(data_path, "r") as f:
                    sensor_1 = np.array(f["RawData/1ee5ec00-4a00-68a1-bb1e-873c2dd4dbde/timestamp"][0])
                    sensor_2 = np.array(f["RawData/1ee5ec03-7e64-6071-8ca3-98dbab0a7719/timestamp"][0])
                    sensor_3 = np.array(f["RawData/1ee5ec04-30cd-678f-a64b-0ce7544ef5e8/timestamp"][0]) 
                    sensor_4 = np.array(f["RawData/1ee5ec04-c845-69e2-853a-25c11543466f/timestamp"][0])
                    sensor_5 = np.array(f["RawData/1ee5ec05-4aea-68f6-ad82-53b16fffae49/timestamp"][0])
                liste_time = [sensor_1, sensor_2, sensor_3, sensor_4, sensor_5]  
                start_time = min(liste_time)
                
            # DONE #

        for subgroup in subgroups:
            # TODO: Save data in to the lists temperature, time and name.
            
            
            
            if probename == "constant":
                list_temperature_1 = []
                list_temperature_2 = []
                list_temperature_3 = []
                list_temperature_4 = []
                list_temperature = [list_temperature_1, list_temperature_2, list_temperature_3, list_temperature_4]

                list_timestamp_1 = []
                list_timestamp_2 = []
                list_timestamp_3 = []
                list_timestamp_4 = []
                list_timestamp = [list_timestamp_1, list_timestamp_2, list_timestamp_3, list_timestamp_4]


                sensor_names = ["sensor_tempCalorimeter1", "sensor_tempCalorimeter2", "sensor_tempCalorimeter3", "sensor_tempEnvironment"]
                for i in range(length):
                    with  h5.File(data_path, "r") as f:
                        temp_1 = []
                        temp_1.append(f["RawData/1ee5ec00-4a00-68a1-bb1e-873c2dd4dbde/temperature"][i])

                        temp_2 = []
                        temp_2.append(f["RawData/1ee5ec03-7e64-6071-8ca3-98dbab0a7719/temperature"][i])

                        temp_3 = []
                        temp_3.append(f["RawData/1ee5ec04-30cd-678f-a64b-0ce7544ef5e8/temperature"][i])

                        temp_4 = []
                        temp_4.append(f["RawData/1ee5ec04-c845-69e2-853a-25c11543466f/temperature"][i])


                        time_1 = []
                        time_1.append(f["RawData/1ee5ec00-4a00-68a1-bb1e-873c2dd4dbde/timestamp"][i]) 

                        time_2 = []
                        time_2.append(f["RawData/1ee5ec03-7e64-6071-8ca3-98dbab0a7719/timestamp"][i])  

                        time_3 = []
                        time_3.append(f["RawData/1ee5ec04-30cd-678f-a64b-0ce7544ef5e8/timestamp"][i])  

                        time_4 = []
                        time_4.append(f["RawData/1ee5ec04-c845-69e2-853a-25c11543466f/timestamp"][i]) 

                    list_temperature_1.append(temp_1)
                    list_temperature_2.append(temp_2)
                    list_temperature_3.append(temp_3)
                    list_temperature_4.append(temp_4)

                    list_timestamp_1.append(time_1)
                    list_timestamp_2.append(time_2)
                    list_timestamp_3.append(time_3)
                    list_timestamp_4.append(time_4)
            
                dict_for_plot = {"temperature": list_temperature, "timestamp": list_timestamp, "name": sensor_names}
                return(dict_for_plot)
                    
            if probename == "probe":
                list_temperature_1 = []
                list_temperature_2 = []
                list_temperature_3 = []
                list_temperature_4 = []
                list_temperature_5 = []
                list_temperature = [list_temperature_1, list_temperature_2, list_temperature_3, list_temperature_4, list_temperature_5]

                list_timestamp_1 = []
                list_timestamp_2 = []
                list_timestamp_3 = []
                list_timestamp_4 = []
                list_timestamp_5 = []
                list_timestamp = [list_timestamp_1, list_timestamp_2, list_timestamp_3, list_timestamp_4, list_timestamp_5]


                sensor_names = ["sensor_tempCalorimeter1", "sensor_tempCalorimeter2", "sensor_tempCalorimeter3", "sensor_tempEnvironment", "sensor_tempHeatingBath"]

                for i in range(length):
                        with  h5.File(data_path, "r") as f:
                            temp_1 = []
                            temp_1.append(f["RawData/1ee5ec00-4a00-68a1-bb1e-873c2dd4dbde/temperature"][i])

                            temp_2 = []
                            temp_2.append(f["RawData/1ee5ec03-7e64-6071-8ca3-98dbab0a7719/temperature"][i])

                            temp_3 = []
                            temp_3.append(f["RawData/1ee5ec04-30cd-678f-a64b-0ce7544ef5e8/temperature"][i])

                            temp_4 = []
                            temp_4.append(f["RawData/1ee5ec04-c845-69e2-853a-25c11543466f/temperature"][i])

                            temp_5 = []
                            temp_5.append(f["RawData/1ee5ec05-4aea-68f6-ad82-53b16fffae49/temperature"][i])



                            time_1 = []
                            time_1.append(f["RawData/1ee5ec00-4a00-68a1-bb1e-873c2dd4dbde/timestamp"][i]) 

                            time_2 = []
                            time_2.append(f["RawData/1ee5ec03-7e64-6071-8ca3-98dbab0a7719/timestamp"][i])  

                            time_3 = []
                            time_3.append(f["RawData/1ee5ec04-30cd-678f-a64b-0ce7544ef5e8/timestamp"][i])  

                            time_4 = []
                            time_4.append(f["RawData/1ee5ec04-c845-69e2-853a-25c11543466f/timestamp"][i])

                            time_5 = []
                            time_5.append(f["RawData/1ee5ec05-4aea-68f6-ad82-53b16fffae49/timestamp"][i])

                    #print("test")
                        list_temperature_1.append(temp_1)
                        list_temperature_2.append(temp_2)
                        list_temperature_3.append(temp_3)
                        list_temperature_4.append(temp_4)
                        list_temperature_5.append(temp_5)

                        list_timestamp_1.append(time_1)
                        list_timestamp_2.append(time_2)
                        list_timestamp_3.append(time_3)
                        list_timestamp_4.append(time_4)
                        list_timestamp_5.append(time_5)

                dict_for_plot = {"temperature": list_temperature, "timestamp": list_timestamp, "name": sensor_names}
                return(dict_for_plot)
    
    # TODO: return the output dict.
   
    

    # DONE #


def cal_mean_and_standard_deviation(data, indexname) -> np.ndarray:
    """Calculating mean and standard deviation for raw data of multiple sensors.

    Args:
        data (np.ndarray): raw data in a 2 dimensional array (m, n), the first dimension should not be 1 if
                           there are multiple measurements at the same time (and place).

    Returns:
        np.ndarray: mean of raw data with standard deviation in a 2D ndarray with shape (2, n).

    """
    # TODO: Calculate the mean and standard deviation of the first dimension and return the result as a
    # two-dimensional (2, n)-shaped ndarray.
    
    if indexname == "index_calorimeter":
        temperature_1c = data[0]
        temperature_2c = data[1]
        temperature_3c = data[2]
        
        liste_for_mid = []
        x = [temperature_1c, temperature_2c, temperature_3c]
        liste_for_std = np.std(x)
        for i in range(len(data[0])):
                mid = []
                mid.append(np.mean([temperature_1c[i], temperature_2c[i], temperature_3c[i]]))
                liste_for_mid.append(mid)
                
      
        mean_temperature_ca = liste_for_mid
        std_temperature_ca = liste_for_std
        array_calorimeter = np.array([mean_temperature_ca, std_temperature_ca])
        return(array_calorimeter)
    
 
    
    if indexname == "index_environment":
        temperature_e = data[0]
        list_temperature_env = [temperature_e]
        mean_temperature_env = np.mean(list_temperature_env)
        std_temperature_env = np.std(list_temperature_env)
        array_environment = np.array([mean_temperature_env, std_temperature_env])
        return(array_environment) 

    # DONE #


def get_start_end_temperature(
    temperature_data):
    """Calculates the high and low temperatures from a dataset.

    This function computes the (average of) the highest temperatures and the (average of) the lowest temperatures
    within a given threshold from the maximum and minimum temperatures recorded in the dataset. These are
    considered as the ending and starting temperatures respectively.

    Args:
        temperature_data (np.ndarray): The temperature dataset as a 2D numpy array.
        threshold (float): The threshold percentage used to identify temperatures close to the maximum
                           and minimum values as high and low temperatures respectively. Default to 0.05

    Returns:
        Tuple[float, float]: A tuple containing the average high temperature first and the average low
                             temperature second.

    """
    # TODO: You don't have to implement this function exactly as docstring expresses it, it just gives
    # an idea that you can refer to. The goal of this function is to obtain from the data the high and
    # low temperatures necessary to calculate the heat capacity.
    
    
    t2_help1 = max(temperature_data[0])
    t2_help2 = max(temperature_data[1])
    t2_help3 = max(temperature_data[2])
    g = [t2_help1, t2_help2, t2_help3]
    t2 = max(g)

    t1_help1 = min(temperature_data[0])
    t1_help2 = min(temperature_data[1])
    t1_help3 = min(temperature_data[2])
    c = [t1_help1, t1_help2, t1_help3]
    t1 = min(c)

    temperature_dataset = [t2, t1]
    return(temperature_dataset)
    
   
    # DONE #
