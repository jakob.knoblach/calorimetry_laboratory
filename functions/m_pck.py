import os
import sys
import time
import json
from w1thermsensor import Sensor, W1ThermSensor
import h5py
import numpy as np


# This if statement enables you to write and run programs that test functions directly at the end of this file.
if __name__ == "__main__":
    import pathlib
    
    file_path = os.path.abspath(__file__)
    file_path = pathlib.Path(file_path)
    root = file_path.parent.parent
    sys.path.append(str(root))
    
from functions import m_json


def check_sensors() -> None:
    
    for sensor in W1ThermSensor.get_available_sensors():
        print("Sensor %s has temperature %.2f" % (sensor.id, sensor.get_temperature()))
    # DONE #


def get_meas_data_calorimetry(metadata: dict) -> dict:

    # Initialize an empty dictionary for storing temperature measurements.
    # The structure is uuid: [[temperatures], [timestamps]].
    data = {i: [[], []] for i in metadata["sensor"]["values"]}
    start = time.time()
    sensor_list = [
        W1ThermSensor(Sensor.DS18B20, id) for id in metadata["sensor"]["serials"]
    ]
    input("Press any key to start measurement... <Ctrl+C> to stop measurement")
    try:
        while True:
            for i, sensor in enumerate(sensor_list):
                # TODO: Get experimental data.
                temperature = sensor.get_temperature()
                data[metadata['sensor']['values'][i]][0].append(temperature)
                data[metadata['sensor']['values'][i]][1].append(time.time() - start)
                print(f"Sensor: {metadata['sensor']['values'][i]}, Temperature: {temperature:.2f}°C")

                # DONE #
            # Print an empty line for better readability in the console.
            print("")
    # Catch the KeyboardInterrupt (e.g., from Ctrl-C) to stop the measurement loop.
    except KeyboardInterrupt:
        # Print the collected data in a formatted JSON structure.
        print(json.dumps(data, indent=4))
    # Always execute the following block.
    finally:
        # Ensure that the lengths of temperature and timestamp lists are the same for each sensor.
        for i in data:
            # If the temperature list is longer, truncate it to match the length of the timestamp list.
            if len(data[i][0]) > len(data[i][1]):
                data[i][0] = data[i][0][0 : len(data[i][1])]
            # If the timestamp list is longer, truncate it to match the length of the temperature list.
            elif len(data[i][0]) < len(data[i][1]):
                data[i][1] = data[i][1][0 : len(data[i][0])]

    return data


def logging_calorimetry(
    data: dict,
    metadata: dict,
    data_folder: str,
    json_folder: str,
) -> None:
    
    # Extract the folder name from the provided path to be used as the H5 file name.
    log_name = data_folder.split("/")[-1]
    # Generate the full path for the H5 file.
    dataset_path = "{}/{}.h5".format(data_folder, log_name)
    # Check and create the logging folder if it doesn't exist.
    if not os.path.exists(data_folder):
        os.makedirs(data_folder)

    # Create a new H5 file.
    f = h5py.File(dataset_path, "w")
    # Create a 'RawData' group inside the H5 file.
    grp_raw = f.create_group("RawData")

    # TODO: Add attribute to HDF5.

    grp_uuid_1 = grp_raw.create_group('1ee81ffd-b187-61ea-931c-5c8753f58271')
    grp_uuid_2 = grp_raw.create_group('1ee81fff-7cb7-68ef-a1ab-ee21fc6623dc')
    grp_uuid_1.attrs ['name'] = 'temperature_cup'
    grp_uuid_1.attrs ['serial'] = '3ce10457a81d'
    grp_uuid_2.attrs ['name'] = 'temperature_environment'
    grp_uuid_2.attrs ['serial'] = '3ce10457dae7'
    
    # DONE #

    # TODO: Write data to HDF5.
    # Gruppen für die UUIDs erstellen und die Attribute temperature_cup & temperature_environment sowie die Serials hinzufügen
    temperature_set_1 = grp_uuid_1.create_dataset('temperature', data=data['1ee81ffd-b187-61ea-931c-5c8753f58271'][0])
    timestamp_set_1 = grp_uuid_1.create_dataset('timestamp', data=data['1ee81ffd-b187-61ea-931c-5c8753f58271'][1])
    temperature_set_2 = grp_uuid_2.create_dataset('temperature', data=data['1ee81fff-7cb7-68ef-a1ab-ee21fc6623dc'][0])
    timestamp_set_2 = grp_uuid_2.create_dataset('timestamp', data=data['1ee81fff-7cb7-68ef-a1ab-ee21fc6623dc'][1])    

    # DONE #

    # Close the H5 file.
    f.close()



if __name__ == "__main__":
    # Test and debug.
    pass
